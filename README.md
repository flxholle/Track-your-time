# Track-your-time
This Table is in german. Every Translation in another language is supported. German:

In diese Tabelle kannst du den Beginn und das Ende einer Tätigkeit eintragen, nach dem Muster HH:MM-HH:MM. Die Tabelle besitzt auch noch eine Auswertung, in der du sehen kannst wann du anfängst, wann du aufhörst und wie lange du im Durchschnitt mit dieser Tätigkeit beschäftigt bist.

Diese Tabelle hat mehrere Untertabellen, dies bitte nicht vergessen.

Am PC: Ihr könnt die Tabellen downloaden, indem ihr auf die entsprechende Datei klickt und dann Download auswählt. Oder oben rechts "Clone or Download" und dann "Download Zip" auswählt.

Am Smartphone: Geht auf "View Code". Dann auf die entsprechende Datei. Dann auf "Open binary file".

Die Datei Notenbild.ods ist für LibreOffice / OpenOffice. Die Datei Notenbild.xlxs ist für Excel 2007 - 2013

Über Fehler bitte informieren

Im Branch "Preversions" werden Testversionen veröffentlicht (sog. Alpha- bzw. Betaversionen).
Alte Versionen im Branch "Old_Versions".

Die Tabelle ist unter GNU General Public License v3.0 veröffentlicht: https://choosealicense.com/licenses/gpl-3.0/
